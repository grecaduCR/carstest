﻿using CarsApplication.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CarsApplication.DataAccess
{
    public class CarsContext: DbContext 
    {
        public CarsContext():base ("CarsContext")
        {

        }

        public DbSet<Cars> Cars { get; set; }

        public DbSet<Brand> Brands { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove();
        }
    }
}