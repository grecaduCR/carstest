﻿namespace CarsApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDescriptionType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Brands", "Description", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Brands", "Description", c => c.Int(nullable: false));
        }
    }
}
