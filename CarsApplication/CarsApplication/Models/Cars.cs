﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarsApplication.Models
{
    public class Cars
    {
        [Key]
        public int Id { get; set; }

        public string Model { get; set; }

        public int Year { get; set; }

        public string  Details { get; set; }

        public virtual Brand Brand { get; set; }

        public int BrandId { get; set; }

    }
}